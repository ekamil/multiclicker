# multiclicker

# Goal

Clicker (increment on click) thingy.

Vocabulary:

- Button: primary building block, an HTML button with a counter. Click increments, right-click decrements
- Container: A group of buttons, with some shared state (for now it's just a sum of counters)
- Containers' container: A group of containers, no shared state

Demo paths:
http://127.0.0.1:5173/demo/components
http://127.0.0.1:5173/demo/containers/gradient
http://127.0.0.1:5173/demo/containers/plain
http://127.0.0.1:5173/demo/containers/plus-minus

## Stack

Created with [SvelteKit](https://kit.svelte.dev/) in 2 days :D

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev
```

## Building

To create a production version of your app:

```bash
npm run build
```

## Deployment
Manual and via Gitlab CI to [surge.sh](https://surge.sh/):

```bash
npm run build && surge ./build military-banana.surge.sh
```
URL: [military-banana](https://military-banana.surge.sh/)

And also [gitlab pages](https://ekamil.gitlab.io/multiclicker/)
